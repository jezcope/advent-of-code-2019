use std::io;
use std::io::BufRead;

use std::iter::successors;

fn main() {
    let stdin = io::stdin();
    let input: Vec<u32> = stdin
        .lock()
        .lines()
        .map(|l| l.unwrap().parse().unwrap())
        .collect();

    let part_1: u32 = input.iter().cloned().map(fuel_for_weight).sum::<u32>();
    println!("Part 1: {}", part_1);

    let part_2: u32 = input
        .iter()
        .cloned()
        .map(total_fuel_for_weight)
        .sum::<u32>();
    println!("Part 2: {}", part_2);
}

fn fuel_for_weight(weight: u32) -> u32 {
    (weight / 3).saturating_sub(2)
}

fn total_fuel_for_weight(weight: u32) -> u32 {
    successors(Some(weight), |&x| Some(fuel_for_weight(x)))
        .skip(1)
        .take_while(|&x| x > 0)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_calculates_module_fuel() {
        assert_eq!(0, fuel_for_weight(2));
        assert_eq!(0, fuel_for_weight(5));
        assert_eq!(2, fuel_for_weight(12));
        assert_eq!(2, fuel_for_weight(14));
        assert_eq!(654, fuel_for_weight(1969));
        assert_eq!(33583, fuel_for_weight(100756));
    }

    #[test]
    fn it_calculates_total_module_fuel() {
        assert_eq!(2, total_fuel_for_weight(14));
        assert_eq!(966, total_fuel_for_weight(1969));
        assert_eq!(50346, total_fuel_for_weight(100756));
    }
}
