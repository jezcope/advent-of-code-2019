use std::collections::{HashMap, HashSet};
use std::io;

#[derive(Clone, Copy)]
enum Dir {
    Up,
    Right,
    Down,
    Left,
}
use Dir::*;

#[derive(Clone, Copy)]
struct Instruction(Dir, isize);

#[derive(Default, Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
struct Point {
    x: isize,
    y: isize,
}

type Circuit = HashMap<Point, isize>;

impl Dir {
    fn from_char(c: char) -> Dir {
        match c {
            'U' => Up,
            'R' => Right,
            'D' => Down,
            'L' => Left,
            _ => panic!("Unexpected direction: {}", c),
        }
    }
}

impl Instruction {
    fn from_str(s: &str) -> Instruction {
        let dir = Dir::from_char(s.chars().next().unwrap());
        let dist = s[1..].parse().unwrap();

        Instruction(dir, dist)
    }

    fn lay_out(self: &Instruction, state: (Point, Circuit, isize)) -> (Point, Circuit, isize) {
        let loc = &state.0;
        let mut wire = state.1;
        let t = state.2;
        let new_wire: Box<dyn Iterator<Item = (Point, isize)>> = match self.0 {
            Up => Box::new(
                (loc.y + 1..=loc.y + self.1)
                    .zip(1..=self.1)
                    .map(|(y, dt)| (Point { x: loc.x, y }, t + dt)),
            ),
            Down => Box::new(
                (loc.y - self.1..loc.y)
                    .rev()
                    .zip(1..=self.1)
                    .map(|(y, dt)| (Point { x: loc.x, y }, t + dt)),
            ),
            Right => Box::new(
                (loc.x + 1..=loc.x + self.1)
                    .zip(1..=self.1)
                    .map(|(x, dt)| (Point { x, y: loc.y }, t + dt)),
            ),
            Left => Box::new(
                (loc.x - self.1..loc.x)
                    .rev()
                    .zip(1..=self.1)
                    .map(|(x, dt)| (Point { x, y: loc.y }, t + dt)),
            ),
        };
        for (p, t) in new_wire {
            wire.entry(p).or_insert(t);
        }
        let new_t = t + self.1;
        let new_loc = match self.0 {
            Up => Point {
                x: loc.x,
                y: loc.y + self.1,
            },
            Down => Point {
                x: loc.x,
                y: loc.y - self.1,
            },
            Right => Point {
                x: loc.x + self.1,
                y: loc.y,
            },
            Left => Point {
                x: loc.x - self.1,
                y: loc.y,
            },
        };

        (new_loc, wire, new_t)
    }
}

fn lay_out_wire(desc: &str) -> Circuit {
    desc.trim()
        .split(',')
        .map(|x| Instruction::from_str(x))
        .fold((Point::default(), Circuit::new(), 0), |acc, x| {
            x.lay_out(acc)
        })
        .1
}

fn find_closest_intersection(wire1: &Circuit, wire2: &Circuit) -> isize {
    let points1: HashSet<&Point> = wire1.keys().collect();
    let points2: HashSet<&Point> = wire2.keys().collect();

    points1
        .intersection(&points2)
        .map(|p| p.x.abs() + p.y.abs())
        .min()
        .unwrap_or(0)
}

fn find_soonest_intersection(wire1: &Circuit, wire2: &Circuit) -> isize {
    let points1: HashSet<&Point> = wire1.keys().collect();
    let points2: HashSet<&Point> = wire2.keys().collect();

    let soonest = points1
        .intersection(&points2)
        .min_by_key(|p| wire1[p] + wire2[p])
        .unwrap();

    wire1[soonest] + wire2[soonest]
}

fn main() {
    let mut in1 = String::new();
    let mut in2 = String::new();
    io::stdin()
        .read_line(&mut in1)
        .expect("Error reading input");
    io::stdin()
        .read_line(&mut in2)
        .expect("Error reading input");

    let wire1 = lay_out_wire(&in1);
    let wire2 = lay_out_wire(&in2);

    let closest = find_closest_intersection(&wire1, &wire2);
    let soonest = find_soonest_intersection(&wire1, &wire2);

    println!("Part 1: {}", closest);
    println!("Part 2: {}", soonest);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_finds_closest_intersection() {
        let wire1 = lay_out_wire("R8,U5,L5,D3");
        let wire2 = lay_out_wire("U7,R6,D4,L4");
        assert_eq!(6, find_closest_intersection(&wire1, &wire2));

        let wire1 = lay_out_wire("R75,D30,R83,U83,L12,D49,R71,U7,L72");
        let wire2 = lay_out_wire("U62,R66,U55,R34,D71,R55,D58,R83");
        assert_eq!(159, find_closest_intersection(&wire1, &wire2));

        let wire1 = lay_out_wire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
        let wire2 = lay_out_wire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
        assert_eq!(135, find_closest_intersection(&wire1, &wire2));
    }

    #[test]
    fn it_finds_soonest_intersection() {
        let wire1 = lay_out_wire("R8,U5,L5,D3");
        let wire2 = lay_out_wire("U7,R6,D4,L4");
        assert_eq!(30, find_soonest_intersection(&wire1, &wire2));

        let wire1 = lay_out_wire("R75,D30,R83,U83,L12,D49,R71,U7,L72");
        let wire2 = lay_out_wire("U62,R66,U55,R34,D71,R55,D58,R83");
        assert_eq!(610, find_soonest_intersection(&wire1, &wire2));

        let wire1 = lay_out_wire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51");
        let wire2 = lay_out_wire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7");
        assert_eq!(410, find_soonest_intersection(&wire1, &wire2));
    }
}
