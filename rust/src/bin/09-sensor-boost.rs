extern crate aoc2019;

use std::io;

use aoc2019::{Memory, IntcodeMachine};

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let program: Memory = input
        .split(',')
        .map(|x| x.trim_end().parse().unwrap())
        .collect();

    let mut machine = IntcodeMachine::new(&program);
    machine.put_input(1);
    machine.run();
    println!("Part 1: {:?}", machine.all_output());

    let mut machine = IntcodeMachine::new(&program);
    machine.put_input(2);
    machine.run();
    println!("Part 2: {:?}", machine.all_output());
}
