use std::cell::RefCell;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use std::rc::{Rc, Weak};

#[derive(Debug)]
struct Mass {
    name: String,
    parent: RefCell<Weak<Mass>>,
    satellites: RefCell<Vec<Weak<Mass>>>,
}

impl Mass {
    pub fn with_name(name: &str) -> Mass {
        Mass {
            name: name.to_string(),
            parent: RefCell::new(Weak::new()),
            satellites: RefCell::new(Vec::new()),
        }
    }

    pub fn add_child(&self, child: &Rc<Mass>) {
        self.satellites.borrow_mut().push(Rc::downgrade(child));
    }

    pub fn set_parent(&self, parent: &Rc<Mass>) {
        self.parent.replace(Rc::downgrade(parent));
    }

    pub fn get_parent(&self) -> Rc<Mass> {
        self.parent.borrow().upgrade().unwrap()
    }

    fn total_orbits(&self, depth: usize) -> usize {
        depth
            + self
                .satellites
                .borrow()
                .iter()
                .map(|m| m.upgrade().unwrap().total_orbits(depth + 1))
                .sum::<usize>()
    }

    fn transfer_to(&self, target: &str) -> usize {
        let satellites = self.satellites.borrow();
        if let Some(n) = satellites
            .iter()
            .filter_map(|m| m.upgrade().unwrap().transfer_downward(target))
            .next()
        {
            n
        } else {
            1 + self.parent.borrow().upgrade().unwrap().transfer_to(target)
        }
    }

    fn transfer_downward(&self, target: &str) -> Option<usize> {
        if self.name == target {
            Some(0)
        } else {
            self.satellites
                .borrow()
                .iter()
                .filter_map(|m| m.upgrade().unwrap().transfer_downward(target))
                .next()
                .map(|n| n + 1)
        }
    }
}

#[derive(Debug)]
struct OrbitalMap {
    centre: Rc<Mass>,
    masses: HashMap<String, Rc<Mass>>,
}

impl OrbitalMap {
    pub fn new() -> OrbitalMap {
        let centre = Rc::new(Mass::with_name("COM"));
        let mut masses = HashMap::new();
        masses.insert("COM".to_string(), Rc::clone(&centre));
        OrbitalMap { centre, masses }
    }

    pub fn from_strings<T>(strings: &mut T) -> OrbitalMap
    where
        T: Iterator<Item = String>,
    {
        let mut map = OrbitalMap::new();
        let masses = &mut map.masses;
        for s in strings {
            let names: Vec<_> = s.split(')').collect();
            let name0 = names[0].to_string();
            let name1 = names[1].to_string();

            let mass0 = masses
                .remove(&name0)
                .unwrap_or_else(|| Rc::new(Mass::with_name(&name0)));
            let mass1 = masses
                .remove(&name1)
                .unwrap_or_else(|| Rc::new(Mass::with_name(&name1)));

            mass0.add_child(&mass1);
            mass1.set_parent(&mass0);

            masses.insert(name0, mass0);
            masses.insert(name1, mass1);
        }

        map
    }

    pub fn total_orbits(&self) -> usize {
        self.centre.total_orbits(0)
    }

    pub fn transfers(&self, from: &str, to: &str) -> usize {
        self.masses[&from.to_string()].get_parent().transfer_to(&to)
    }
}

fn main() {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines().filter_map(io::Result::ok);
    let map = OrbitalMap::from_strings(&mut lines);

    println!("Part 1: {}", map.total_orbits());
    println!("Part 2: {}", map.transfers("YOU", "SAN"));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_parses_orbital_maps() {
        let mut input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L"
            .split_whitespace()
            .map(str::to_string);
        let map = OrbitalMap::from_strings(&mut input);

        assert_eq!(42, map.total_orbits());
    }

    #[test]
    fn it_calculates_orbital_transfers() {
        let mut input = "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN"
            .split_whitespace()
            .map(str::to_string);
        let map = OrbitalMap::from_strings(&mut input);

        assert_eq!(4, map.transfers("YOU", "SAN"));
    }

    #[test]
    fn it_calculates_null_orbital_transfers() {
        let mut input = "COM)A\nA)YOU\nA)SAN".split_whitespace().map(str::to_string);
        let map = OrbitalMap::from_strings(&mut input);

        assert_eq!(0, map.transfers("YOU", "SAN"));
    }

    #[test]
    fn it_calculates_downward_orbital_transfers() {
        let mut input = "COM)A\nA)YOU\nA)B\nB)SAN"
            .split_whitespace()
            .map(str::to_string);
        let map = OrbitalMap::from_strings(&mut input);

        assert_eq!(1, map.transfers("YOU", "SAN"));
    }
}
