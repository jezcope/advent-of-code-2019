extern crate aoc2019;

use std::io;

use aoc2019::{IntcodeMachine, Memory, Word};

const AIR_CONDITIONER: Word = 1;
const THERMAL_RADIATOR: Word = 5;

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let program: Memory = input
        .split(',')
        .map(|x| x.trim_end().parse().unwrap())
        .collect();

    let mut machine = IntcodeMachine::new(&program);
    machine.put_input(AIR_CONDITIONER);
    machine.run();

    println!("Part 1: {}", machine.out_queue.back().expect("No output!"));

    let mut machine = IntcodeMachine::new(&program);
    machine.put_input(THERMAL_RADIATOR);
    machine.run();

    println!("Part 1: {}", machine.out_queue.back().expect("No output!"));
}
