use std::io;

const WIDTH: usize = 25;
const HEIGHT: usize = 6;
const LAYER_SIZE: usize = WIDTH * HEIGHT;

type Layer = Vec<char>;
type Image = Vec<Layer>;

fn read_image(chars: &[char]) -> Image {
    chars.chunks(LAYER_SIZE).map(|x| x.to_vec()).collect()
}

fn image_checksum(image: &Image) -> usize {
    if let Some(layer) = image.iter()
        .min_by_key(|l| l.iter().filter(|&&x| x == '0').count())
    {
        let ones = layer.iter().filter(|&&x| x == '1').count();
        let twos = layer.iter().filter(|&&x| x == '2').count();

        ones * twos
    } else {
        0
    }
}

fn decode_image(image: &Image) -> Layer {
    let mut layers = image.iter().rev();
    let mut result = layers.next().unwrap().clone();

    for l in layers {
        result = result.into_iter().zip(l)
            .map(|(old, &new)| {
                match new {
                    '2' => old,
                    _ => new,
                }
            }).collect();
    }

    result
}

fn display_layer(layer: &Layer) {
    for i in 0..HEIGHT {
        for j in 0..WIDTH {
            let c = match layer[i*WIDTH + j] {
                '0' => ' ',
                '1' => '█',
                _ => 'X',
            };
            print!("{}", c);
        }
        println!();
    }
}

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let chars: Vec<_> = input.trim().chars().collect();
    let image = read_image(&chars);
    let check = image_checksum(&image);

    println!("Part 1: {}", check);

    let decoded = decode_image(&image);
    println!("Part 2:");
    display_layer(&decoded);
}
