use std::io;

fn is_monotonic(digits: &str) -> bool {
    let digits0 = digits.chars();
    let digits1 = digits.chars().skip(1);

    digits0.zip(digits1).all(|(a, b)| a <= b)
}

fn has_a_pair(digits: &str) -> bool {
    let digits0 = digits.chars();
    let digits1 = digits.chars().skip(1);

    digits0.zip(digits1).any(|(a, b)| a == b)
}

fn validate(password: usize) -> Option<usize> {
    let digits = password.to_string();

    if is_monotonic(&digits) && has_a_pair(&digits) {
        Some(password)
    } else {
        None
    }
}

fn has_a_lone_pair(digits: &str) -> bool {
    let mut runs = Vec::with_capacity(digits.len());
    let mut count = 0;
    let mut last = None;

    for current in digits.chars() {
        if Some(current) == last {
            count += 1;
        } else {
            runs.push((last, count));
            last = Some(current);
            count = 1;
        }
    }
    runs.push((last, count));

    runs.iter().any(|(_, count)| *count == 2)
}

fn validate_full(password: usize) -> Option<usize> {
    let digits = password.to_string();

    if is_monotonic(&digits) && has_a_lone_pair(&digits) {
        Some(password)
    } else {
        None
    }
}

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let range: Vec<usize> = input
        .trim_end()
        .split('-')
        .map(|x| x.parse().unwrap())
        .collect();

    assert_eq!(2, range.len());
    let (lo, hi) = (range[0], range[1]);

    let result1 = (lo..=hi).filter_map(validate).count();
    println!("Part 1: {}", result1);

    let result2 = (lo..=hi).filter_map(validate_full).count();
    println!("Part 2: {}", result2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_tests_for_monotonicity() {
        assert_eq!(true, is_monotonic("112345"));
        assert_eq!(true, is_monotonic("123456"));
        assert_eq!(true, is_monotonic("111122"));
        assert_eq!(true, is_monotonic("123444"));
        assert_eq!(false, is_monotonic("223450"));
    }

    #[test]
    fn it_tests_for_pairs() {
        assert_eq!(true, has_a_pair("112345"));
        assert_eq!(false, has_a_pair("123456"));
        assert_eq!(true, has_a_pair("111122"));
        assert_eq!(true, has_a_pair("123444"));
        assert_eq!(true, has_a_pair("223450"));
    }

    #[test]
    fn it_tests_pairs_outside_groups() {
        assert_eq!(true, has_a_lone_pair("112345"));
        assert_eq!(false, has_a_lone_pair("123456"));
        assert_eq!(true, has_a_lone_pair("111122"));
        assert_eq!(false, has_a_lone_pair("123444"));
        assert_eq!(true, has_a_lone_pair("223450"));
    }

    #[test]
    fn it_tests_passwords() {
        assert_eq!(None, validate(223450));
        assert_eq!(None, validate(123789));
        assert_eq!(Some(112345), validate(112345));
        assert_eq!(Some(111123), validate(111123));
    }

    #[test]
    fn it_tests_passwords_more() {
        assert_eq!(None, validate_full(223450));
        assert_eq!(None, validate_full(123789));
        assert_eq!(None, validate_full(111123));
        assert_eq!(None, validate_full(123444));
        assert_eq!(Some(112345), validate_full(112345));
        assert_eq!(Some(111122), validate_full(111122));
    }
}
