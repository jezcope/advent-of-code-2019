extern crate aoc2019;

use std::fmt::Debug;
use std::io;

use aoc2019::{IntcodeMachine, Memory, MemoryStatic, Word};

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let program: Memory = input
        .split(',')
        .map(|x| x.trim_end().parse().unwrap())
        .collect();

    let max_signal =
        find_max_signal(&program, run_with_settings, 0..=4).expect("No maximum signal found");
    println!("Part 1: {}", max_signal);

    let max_signal =
        find_max_signal(&program, run_loop_with_settings, 5..=9).expect("No maximum signal found");
    println!("Part 2: {}", max_signal);
}

fn run_with_settings(program: &MemoryStatic, settings: &[Word]) -> Word {
    let mut output = 0;

    for setting in settings {
        let mut machine = IntcodeMachine::new(program);
        machine.put_input(*setting);
        machine.put_input(output);
        machine.run();
        output = machine.pop_output().unwrap();
    }

    output
}

fn run_loop_with_settings(program: &MemoryStatic, settings: &[Word]) -> Word {
    let mut output = 0;

    let mut machines: Vec<_> = settings
        .iter()
        .map(|&s| {
            let mut machine = IntcodeMachine::new(program);
            machine.put_input(s);
            machine
        })
        .collect();

    'outer: loop {
        for machine in &mut machines {
            machine.put_input(output);
            machine.run_until_output();
            match machine.pop_output() {
                Some(i) => output = i,
                None => break 'outer,
            }
        }
    }

    output
}

fn find_max_signal<F, I>(program: &MemoryStatic, run: F, range: I) -> Option<Word>
where
    F: Fn(&MemoryStatic, &[Word]) -> Word,
    I: Iterator<Item = Word>,
{
    compute_permutations(range.collect())
        .into_iter()
        .map(|perm| run(program, &*perm))
        .max()
}

fn compute_permutations<T>(from: Vec<T>) -> Vec<Vec<T>>
where
    T: PartialEq + Copy + Debug,
{
    let mut result = Vec::new();

    if from.len() == 1 {
        result.push(from);
    } else {
        for i in &from {
            let remainder = from.iter().cloned().filter(|j| j != i).collect();
            for mut p in compute_permutations(remainder) {
                p.push(*i);
                result.push(p);
            }
        }
    }

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_computes_thruster_signal() {
        assert_eq!(
            43210,
            run_with_settings(
                &vec![3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0],
                &[4, 3, 2, 1, 0]
            )
        );
        assert_eq!(
            54321,
            run_with_settings(
                &vec![
                    3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23,
                    23, 4, 23, 99, 0, 0
                ],
                &[0, 1, 2, 3, 4]
            )
        );
        assert_eq!(
            65210,
            run_with_settings(
                &vec![
                    3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7,
                    33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0
                ],
                &[1, 0, 4, 3, 2]
            )
        );
    }

    #[test]
    fn it_finds_max_thruster_signal() {
        assert_eq!(
            Some(43210),
            find_max_signal(
                &vec![3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0],
                run_with_settings,
                0..=4
            )
        );
        assert_eq!(
            Some(54321),
            find_max_signal(
                &vec![
                    3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23,
                    23, 4, 23, 99, 0, 0
                ],
                run_with_settings,
                0..=4
            )
        );
        assert_eq!(
            Some(65210),
            find_max_signal(
                &vec![
                    3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7,
                    33, 1, 33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0
                ],
                run_with_settings,
                0..=4
            )
        );
    }

    #[test]
    fn it_computes_looped_thruster_signal() {
        assert_eq!(
            139629729,
            run_loop_with_settings(
                &vec![
                    3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001,
                    28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
                ],
                &[9, 8, 7, 6, 5]
            )
        );
        assert_eq!(
            18216,
            run_loop_with_settings(
                &vec![
                    3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26,
                    1001, 54, -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55,
                    2, 53, 55, 53, 4, 53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10
                ],
                &[9, 7, 8, 5, 6]
            )
        );
    }

    #[test]
    fn it_finds_max_looped_thruster_signal() {
        assert_eq!(
            Some(139629729),
            find_max_signal(
                &vec![
                    3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001,
                    28, -1, 28, 1005, 28, 6, 99, 0, 0, 5
                ],
                run_loop_with_settings,
                5..=9
            )
        );
        assert_eq!(
            Some(18216),
            find_max_signal(
                &vec![
                    3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26,
                    1001, 54, -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55,
                    2, 53, 55, 53, 4, 53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10
                ],
                run_loop_with_settings,
                5..=9
            )
        );
    }
}
