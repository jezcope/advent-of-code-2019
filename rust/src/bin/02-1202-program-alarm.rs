extern crate aoc2019;

use std::io;

use aoc2019::{run_intcode, Memory, ProgResult, Word};

fn main() {
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Error reading input");

    let program: Memory = input
        .split(',')
        .map(|x| x.trim_end().parse().unwrap())
        .collect();

    println!(
        "Part 1: {}",
        run_with_input(&program, 12, 2).expect("Program error")
    );

    println!(
        "Part 2: {:?}",
        find_inputs(&program, 19_690_720, 0, 100).unwrap()
    );
}

fn run_with_input(prog: &Memory, noun: Word, verb: Word) -> ProgResult {
    let mut mem = prog.to_owned();
    mem[1] = noun;
    mem[2] = verb;

    run_intcode(&mut mem)
}

fn find_inputs(prog: &Memory, target: Word, lo: Word, hi: Word) -> ProgResult {
    for x in lo..hi {
        for y in lo..hi {
            let output = run_with_input(&prog, x, y)?;
            if output == target {
                return Ok(100 * x + y);
            }
        }
    }

    Err("None found".to_string())
}
