use std::collections::VecDeque;
use std::ops::{Index, IndexMut, Deref, DerefMut};
use std::iter::FromIterator;

pub type Addr = usize;
pub type Word = i64;
pub type MemoryStatic = [Word];

#[derive(Debug, Clone, Default)]
pub struct Memory {
    inner: Vec<Word>,
}

impl Memory {
    pub fn with_contents(init: &MemoryStatic) -> Memory {
        Memory{ inner: init.to_vec() }
    }
}

impl Deref for Memory {
    type Target = Vec<Word>;

    fn deref(&self) -> &Vec<Word> {
        &self.inner
    }
}

impl DerefMut for Memory {
    fn deref_mut(&mut self) -> &mut Vec<Word> {
        &mut self.inner
    }
}

impl PartialEq<Vec<Word>> for Memory {
    fn eq(&self, other: &Vec<Word>) -> bool {
        self.inner == *other
    }
}

impl PartialEq<Memory> for Vec<Word> {
    fn eq(&self, other: &Memory) -> bool {
        other.inner == *self
    }
}

impl Index<Addr> for Memory {
    type Output = Word;

    fn index(&self, index: Addr) -> &Word {
        if let Some(x) = self.inner.get(index) {
            x
        } else {
            &0
        }
    }
}

impl IndexMut<Addr> for Memory {
    fn index_mut(&mut self, index: Addr) -> &mut Word {
        if self.inner.len() < index + 1 {
            self.inner.resize(index + 1, 0);
        }

        self.inner.get_mut(index).unwrap()
    }
}

impl From<Vec<Word>> for Memory {
    fn from(other: Vec<Word>) -> Memory {
        Memory{ inner: other }
    }
}

impl FromIterator<Word> for Memory {
    fn from_iter<I>(iter: I) -> Memory
    where I: IntoIterator<Item=Word>
    {
        Memory{ inner: iter.into_iter().collect() }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Param {
    Pos,
    Imm,
    Rel,
}
use Param::*;

pub type ProgResult = Result<Word, String>;

impl From<Word> for Param {
    fn from(n: Word) -> Param {
        match n {
            0 => Pos,
            1 => Imm,
            2 => Rel,
            _ => panic!("Unrecognised parameter mode {}", n),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Instruction {
    Add(Param, Param, Param),
    Mul(Param, Param, Param),
    In(Param),
    Out(Param),
    JumpTrue(Param, Param),
    JumpFalse(Param, Param),
    LessThan(Param, Param, Param),
    Equal(Param, Param, Param),
    RelBase(Param),
    Halt,
}
use Instruction::*;

impl Instruction {
    fn from_int(raw: Word) -> Instruction {
        let opcode = raw % 100;
        let mode1 = Param::from((raw / 100) % 10);
        let mode2 = Param::from((raw / 1000) % 10);
        let mode3 = Param::from((raw / 10000) % 10);

        match opcode {
            1 => Add(mode1, mode2, mode3),
            2 => Mul(mode1, mode2, mode3),
            3 => In(mode1),
            4 => Out(mode1),
            5 => JumpTrue(mode1, mode2),
            6 => JumpFalse(mode1, mode2),
            7 => LessThan(mode1, mode2, mode3),
            8 => Equal(mode1, mode2, mode3),
            9 => RelBase(mode1),

            99 => Halt,
            _ => panic!("Opcode {} not implemented", opcode),
        }
    }
}

pub struct IntcodeMachine {
    pub memory: Memory,
    pub ip: Addr,
    pub rb: Word,
    pub in_queue: VecDeque<Word>,
    pub out_queue: VecDeque<Word>,
    pub halted: bool,
}

impl IntcodeMachine {
    pub fn new(program: &MemoryStatic) -> IntcodeMachine {
        IntcodeMachine
        {
            memory: Memory::with_contents(program),
            ip: 0,
            rb: 0,
            in_queue: VecDeque::new(),
            out_queue: VecDeque::new(),
            halted: false,
        }
    }

    pub fn run(&mut self) {
        loop {
            let i = Instruction::from_int(self.memory[self.ip]);
            if !self.exec_instruction(i) {
                break;
            }
        }
    }

    pub fn run_until_output(&mut self) {
        while !self.has_output() {
            let i = Instruction::from_int(self.memory[self.ip]);
            if !self.exec_instruction(i) {
                break;
            }
        }
    }

    pub fn put_input(&mut self, x: Word) {
        self.in_queue.push_back(x);
    }

    pub fn has_output(&self) -> bool {
        !self.out_queue.is_empty()
    }

    pub fn pop_output(&mut self) -> Option<Word> {
        self.out_queue.pop_front()
    }

    pub fn all_output(&mut self) -> Vec<Word> {
        self.out_queue.drain(..).collect()
    }

    pub fn is_halted(&self) -> bool {
        self.halted
    }

    fn get_mem(&self, p: Param, i: Addr) -> Word {
        let x = self.memory[self.ip + i];
        match p {
            Pos => self.memory[x as usize],
            Imm => x,
            Rel => self.memory[(x + self.rb) as usize],
        }
    }

    fn put_mem(&mut self, p: Param, i: Addr, val: Word) {
        let x = self.memory[self.ip + i];
        match p {
            Pos => self.memory[x as usize] = val,
            Imm => panic!("Cannot write to an immediate parameter"),
            Rel => self.memory[(x + self.rb) as usize] = val,
        }
    }

    fn exec_instruction(&mut self, i: Instruction) -> bool {
        self.halted = match i {
            Add(p1, p2, p3) => {
                let in1 = self.get_mem(p1, 1);
                let in2 = self.get_mem(p2, 2);
                self.put_mem(p3, 3, in1 + in2);

                self.ip += 4;
                true
            }
            Mul(p1, p2, p3) => {
                let in1 = self.get_mem(p1, 1);
                let in2 = self.get_mem(p2, 2);
                self.put_mem(p3, 3, in1 * in2);

                self.ip += 4;
                true
            }
            LessThan(p1, p2, p3) => {
                let in1 = self.get_mem(p1, 1);
                let in2 = self.get_mem(p2, 2);
                self.put_mem(p3, 3, (in1 < in2) as Word);

                self.ip += 4;
                true
            }
            Equal(p1, p2, p3) => {
                let in1 = self.get_mem(p1, 1);
                let in2 = self.get_mem(p2, 2);
                self.put_mem(p3, 3, (in1 == in2) as Word);

                self.ip += 4;
                true
            }
            In(p) => {
                let x = self.in_queue.pop_front().unwrap();
                self.put_mem(p, 1, x);
                self.ip += 2;
                true
            }
            Out(p) => {
                let x = self.get_mem(p, 1);
                self.out_queue.push_back(x);
                self.ip += 2;
                true
            }
            JumpTrue(p1, p2) => {
                let test = self.get_mem(p1, 1) != 0;
                self.ip = if test {
                    self.get_mem(p2, 2) as Addr
                } else {
                    self.ip + 3
                };
                true
            }
            JumpFalse(p1, p2) => {
                let test = self.get_mem(p1, 1) != 0;
                self.ip = if !test {
                    self.get_mem(p2, 2) as Addr
                } else {
                    self.ip + 3
                };
                true
            },
            RelBase(p1) => {
                let adj = self.get_mem(p1, 1);
                self.rb += adj;
                self.ip += 2;
                true
            }

            Halt => false,
        };

        self.halted
    }

    pub fn result(&self) -> Word {
        self.memory[0]
    }
}

pub fn run_intcode(program: &Memory) -> ProgResult {
    let mut machine = IntcodeMachine::new(program);
    machine.run();
    Ok(machine.result())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_runs_day02_programs() {
        let prog1 = vec![1, 0, 0, 0, 99];
        let prog2 = vec![2, 3, 0, 3, 99];
        let prog3 = vec![2, 4, 4, 5, 99, 0];
        let prog4 = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];

        let mut machine = IntcodeMachine::new(&prog1);
        machine.run();
        assert_eq!(2, machine.result());
        assert_eq!(vec![2, 0, 0, 0, 99], machine.memory);

        let mut machine = IntcodeMachine::new(&prog2);
        machine.run();
        assert_eq!(2, machine.result());
        assert_eq!(vec![2, 3, 0, 6, 99], machine.memory);

        let mut machine = IntcodeMachine::new(&prog3);
        machine.run();
        assert_eq!(2, machine.result());
        assert_eq!(vec![2, 4, 4, 5, 99, 9801], machine.memory);

        let mut machine = IntcodeMachine::new(&prog4);
        machine.run();
        assert_eq!(30, machine.result());
        assert_eq!(vec![30, 1, 1, 4, 2, 5, 6, 0, 99], machine.memory);
    }

    #[test]
    fn it_performs_io() {
        let mut machine = IntcodeMachine::new(&vec![3, 0, 4, 0, 99]);
        machine.put_input(34);
        machine.run();
        assert_eq!(Some(34), machine.pop_output());
    }

    #[test]
    fn it_groks_param_modes() {
        let mut machine = IntcodeMachine::new(&vec![1002, 4, 3, 4, 33]);
        machine.run();
        assert_eq!(vec![1002, 4, 3, 4, 99], machine.memory);
    }

    #[test]
    fn it_groks_relative_parameters() {
        let prog1 = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
        let mut machine = IntcodeMachine::new(&prog1);
        machine.run();
        assert_eq!(prog1, machine.all_output());

        let prog2 = vec![1102,34915192,34915192,7,4,7,99,0];
        let mut machine = IntcodeMachine::new(&prog2);
        machine.run();
        assert_eq!(16f64, (machine.pop_output().unwrap() as f64).log10().ceil());

        let prog3 = vec![104,1125899906842624,99];
        let mut machine = IntcodeMachine::new(&prog3);
        machine.run();
        assert_eq!(1125899906842624, machine.pop_output().unwrap());
    }

    // TODO: Document test new opcodes from Day 5 https://adventofcode.com/2019/day/5#part2
}
